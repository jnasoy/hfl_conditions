<?php
// Start of display all conditions function
function hfl_conditions(){
	global $wpdb;
	$table_name =$wpdb->prefix.'conditions' ;
	$hfl_ac = hfl_read_all($table_name);
	if($hfl_ac):
?>
<style>
	tr:nth-child(even){
		background-color:#fff;
	}
</style>
	<table cellpadding="10" style="border-collapse: collapse; box-shadow: 0 9px 23px 0 #7f7f7f, 0 5px 5px 0 #999;padding:1em 2em;width:auto;float:left;margin:2em 2em 0 1em;">
		<tr style="font-size:1.15em;color:#111;background-color:#aaa; border:1px solid #aaa;">
			<th style="width:300px;background-color:#009688;color:#FFF;border-right: 1px solid #aaa;">Conditions</th>
			<th style="width:600px;background-color:#009688;color:#FFF;">Description</th>
		</tr>
<?php foreach($hfl_ac as $conditions): ?>
		<tr style="border:1px solid #aaa;">
			<td style="border:1px solid #aaa;text-align: center;"><?php echo $conditions->name;?></td>
			<td><?php echo $conditions->descr; ?></td>
		</tr>
<?php endforeach; ?>
	</table>
<?php else: ?>
	<h3>No data yet.</h3>
<?php
endif;
// End of display all conditions function
}
// Start of add condition function
function hfl_add_condition()
{
?>
<div class='main_frame'>
	<h2>Add Condition</h2>
	<form style=" box-shadow: 0 9px 23px 0 #7f7f7f, 0 5px 5px 0 #999;background-color: #009688;color:#FFF;padding:1em 2em;width:auto;float:left;"action="<?php echo site_url( 'wp-content/plugins/HFL_conditions/hfl_conditions_actions.php', __FILE__ ); ?>" method="post" accept-charset="utf-8">

		<div class="frame">
			<label for="hflc_cname">Name</label><br>
			<input type="text" name="add[condition]" placeholder="......." required>
		</div>
		<div>
			<label for="hflc_description"> Description</label><br>
			<textarea style="width: 500px;height: 250px;padding:.5em;resize: none;"name="add[description]" placeholder="........." required ></textarea>
		</div>
		<div>
			<span><?php hflc_check(); ?><button style="border:none;padding:.5em 1em ;float:right;color:#fff;background-color: #00695C;border:1px solid #fff;">Save</button></span>
		</div>
	</form>
</div>
<?php  }
// End of add condition function
?>
<?php
function hflc_check(){
	/* Form Validation*/
	if(isset($_GET['m'])):
		if($_GET['m'] == 1): ?>
			<h5 style="background-color:#00C853;padding:.5em;float:left;width:85%;margin-top:4px;">Success!</h5>
	<?php else:?>
			<h5 style="background-color:#E53935;padding:.5em;float:left;width:85%;margin-top:4px;">Something's wrong. Please try again</h5>
	<?php endif; //end ifelse
	endif;//end isset
}
function hfl_read_all($table_name){
	global $wpdb;
	$table 	= $table_name;
	$results = $wpdb->get_results( "SELECT * FROM $table ORDER BY name ASC" );
	return $results;
}
function hfl_update_condition(){
	global $wpdb;
	$tbl = $wpdb->prefix.'conditions';
	$query_result=hfl_read_all($tbl);
	if ($query_result):
	?>
	<div class='main_frame'>
	<h2>Update Condition</h2>
	<form style=" box-shadow: 0 9px 23px 0 #7f7f7f, 0 5px 5px 0 #999;background-color: #009688;color:#FFF;padding:1em 2em;width:auto;float:left;"action="<?php echo site_url( 'wp-admin/admin.php?page=hfl-condition-update'); ?>" method="post" accept-charset="utf-8">

		<span class="frame">
			<div>
				<label for="hflc_cname">Select</label><br>
				<select id="conselect" name="choose" onchange="this.form.submit()">
					<option value="" disabled selected>Select Condition</option>
					<?php
						foreach($query_result as $conditions){
							echo '<option value="'.$conditions->name.'">'.$conditions->name.'</option>';
							}
						?>
				</select>
			</div>
		</span>
	</form>

</div>
<!-- EDIT FORM -->
<?php
$up_var = $_POST['choose'];
if ($up_var) {
	global $wpdb;
	$tblname =$wpdb->prefix.'conditions' ;
	$up_desc =$wpdb->get_results("SELECT * FROM $tblname WHERE name = '$up_var'");
	// echo $up_desc[0]->ID;

	?>

	<form style=" margin-left:1em;box-shadow: 0 9px 23px 0 #7f7f7f, 0 5px 5px 0 #999;background-color: #009688;color:#FFF;padding:1em 2em;width:auto;float:left;"action="<?php echo site_url( 'wp-content/plugins/HFL_conditions/hfl_conditions_actions.php', __FILE__ ); ?>" method="post" accept-charset="utf-8">

		<div class="frame">
			<label for="hflc_cname">Name</label><br>
			<input type="hidden" name="update[id]" value="<?php echo $up_desc[0]->id;?>">
			<input type="text" name="update[condition]" value="<?php echo $up_var;?>" placeholder="......." required>
		</div>
		<div>
			<label for="hflc_description"> Description</label><br>
			<textarea style="width: 500px;height: 250px;padding:.5em;resize: none;"name="update[description]" placeholder="........." required ><?php if($up_desc)echo $up_desc[0]->descr;?></textarea>
		</div>
		<div>
			<span><?php hflc_check(); ?><button style="border:none;padding:.5em 1em ;float:right;color:#fff;background-color: #00695C;border:1px solid #fff;">Update</button></span>
		</div>
	</form>
<?php } ?>

 <!-- END OF EDIT FORM -->
<?php else: ?>
	<h2>No data yet!</h2>

<?php
endif;
}
function hfl_delete_condition(){
	global $wpdb;
	$tbl = $wpdb->prefix.'conditions';
	$query_result=hfl_read_all($tbl);
	if ($query_result):
	?>
	<div class='main_frame'>
	<h2>Delete Condition</h2>
	<form style=" box-shadow: 0 9px 23px 0 #7f7f7f, 0 5px 5px 0 #999;background-color: #009688;color:#FFF;padding:1em 2em;width:auto;float:left;"action="<?php echo site_url( 'wp-content/plugins/HFL_conditions/hfl_conditions_actions.php', __FILE__ ); ?>" method="post" accept-charset="utf-8">
		<span class="frame">
			<div>
				<label for="hflc_cname">Select</label><br>
				<select id="conselect" name="delete[choose]" onchange="selectFunction()">
					<option value="" disabled selected>Select a condition</option>
					<?php
						foreach($query_result as $conditions){
							echo '<option value="'.$conditions->name.'">'.$conditions->name.'</option>';
							}
						?>
				</select>
				<button style="margin:0 0 0 .5em;padding:.4em 1em ;float:right;color:#fff;background-color: #EF5350;border:1px solid #fff;" onclick="return confirm('Are you sure?');">Delete</button>
			</div>
		</span><br>
		<?php hflc_check();?>
	</form>
</div>
<?php else: ?>
	<h2>No data yet!</h2>

<?php
endif;
}
?>
