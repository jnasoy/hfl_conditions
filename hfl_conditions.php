<?php
/*
Plugin Name: HFL Conditions
Plugin URI: http://exdigita.com/
Description: Manages the conditions related to anti aging.
Version: 1.0.0
Author: NSH
Author URI: http://exdigita.com/
*/
include ('includes/hfl_conditions_functions.php');
define('PLUG_URL', plugins_url('',__FILE__));

// CREATE TABLE UPON ACTIVATION
function activate_createdb(){
  global $wpdb;

  $table = $wpdb ->prefix.'conditions';

  $charset_collate = $wpdb->get_charset_collate();

  $sql = "CREATE TABLE $table (
            id int(11) NOT NULL AUTO_INCREMENT,
            name varchar(50) DEFAULT NULL,
            descr varchar(4000) DEFAULT NULL,
            UNIQUE KEY id (id)
          ) $charset_collate;";


          require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
          dbDelta( $sql );
}
register_activation_hook( __FILE__, 'activate_createdb' );
// END OF CREATING TABLE
// 
if ( is_admin() ) {
	add_action('admin_menu', 'hfl_conditions_setup_menu');
}
function hfl_conditions_setup_menu(){
    add_menu_page( 'HFL Conditions', 'HFL Conditions', 'manage_options', 'hfl-conditions', 'hfl_show_option',  plugins_url('/HFL_conditions/img/HFL-conditions-icon.png',1) );
    add_submenu_page( 'hfl-conditions', '', 'Conditions', 'manage_options', 'hfl-conditions', 'hfl_show_option');
    add_submenu_page('hfl-conditions', 'Add Condition', 'Add Condition', 'manage_options','hfl-condition-entry', 'hfl_add_option');
    add_submenu_page('hfl-conditions', 'Update Condition', 'Update Condition', 'manage_options','hfl-condition-update', 'hfl_update_option');
	add_submenu_page('hfl-conditions', 'Delete Condition', 'Delete Condition', 'manage_options','hfl-condition-delete', 'hfl_delete_option');
}
function hfl_show_option(){
	hfl_conditions();
}
function hfl_add_option(){
	if ( !current_user_can( 'manage_options' ) )  {
				wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
			}
			hfl_add_condition();
		}
function hfl_update_option(){
	hfl_update_condition();
	}
function hfl_delete_option(){
	hfl_delete_condition();
	}
 ?>
