<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-load.php';
$site = site_url();
$msg = 0;
if(isset($_POST['add'])){
	$url_part = '/wp-admin/admin.php?page=hfl-condition-entry&m=';
	$add = $_POST['add'];
	$msg = hfl_validate($add);
	if($msg == 1){
		$msg=hfl_check_duplicate($add);
	}
}
elseif (isset($_POST['update'])) {
	$url_part = '/wp-admin/admin.php?page=hfl-condition-update&m=';
	$cupdate = $_POST['update'];
	if ($cupdate) {
		$msg=hfl_update($cupdate);
	}
}
elseif (isset($_POST['delete'])) {
	$url_part = '/wp-admin/admin.php?page=hfl-condition-delete&m=';
	$cdelete=$_POST['delete'];
	if ($cdelete['choose']){
		$msg=hfl_delete($cdelete);
	}
}
$url = 'Location: ' . $site . $url_part . $msg;
header($url);

// FUNCTIONS

// ADD DATA
function hfl_create_condition($add){
	global $wpdb;
	$msg = 0;
			$cname 		= trim($add['condition']);
			$desc 		= trim($add['description']);
			$table 		= $wpdb->prefix.'conditions';
	$args_val = array(
			'name' 	=> $cname,
			'descr' => $desc
				);
	$args_form	= array(
			'%s',
			'%s'
				);
	$wpdb->insert( $table, $args_val, $args_form );
	if($wpdb->insert_id){
		$msg = 1;
	}
	return $msg;
}
// DELETE DATA
function hfl_delete($cdelete){
	global $wpdb;
	$msg=1;
	$cdel=$cdelete['choose'];
	$table= $wpdb->prefix.'conditions';
	$where_var = array('name'=>$cdel);
	$wpdb->delete($table,$where_var );
	return $msg;
}
// UPDATE DATA
function hfl_update($cupdate){
	global $wpdb;
	$msg=1;
	$table = $wpdb->prefix.'conditions';
	$cid=$cupdate['id'];
	$cupd=trim($cupdate['condition']);
	$dupd=trim($cupdate['description']);
	$up_array = array('name' =>$cupd,'descr' =>$dupd);
	$where_var=array('id'=>$cid);
	$wpdb->update($table,$up_array,$where_var);
	return $msg;
}
// Validation
function hfl_validate($array){
	$msg = 0;
	foreach($array as $key => $val){
		$val = trim($val);
		if(empty($val) ){
			$msg = 0;
			break;
		}elseif($key == 'condition'){
			$msg = 1;
		}
	}
	return $msg;
}
// The function below is for checking existing condition name
function hfl_check_duplicate($add){
	global $wpdb;
	$table_name=$wpdb->prefix.'conditions';
	$wpdb->get_results( "SELECT id FROM $table_name where name='".$add["condition"]."'" );
	$row_check=$wpdb->num_rows;
	if ($row_check>0) {
	$msg=0;
	echo "bad" . $msg;
	return $msg;
	}
	else{
	$msg=hfl_create_condition($add);
	return $msg;
	}
}
?>